import React, { FunctionComponent } from 'react'

import './FilterBar.scss'

interface FilterBarProperties {
    value: string
    onFilterChanged: (value: string) => void
}

export const FilterBar: FunctionComponent<FilterBarProperties> = ({ value, onFilterChanged }) => {
    return <div className="filter-bar">
        <input type="text" value={value} placeholder="Filter by Keyword" onChange={e => onFilterChanged(e.target.value)} />
        <button onClick={() => onFilterChanged('')} type="button" data-testid="resetFilterButton">
            ❌
        </button>
    </div>
}
import React, { FunctionComponent } from 'react'
import { Feature } from '../../types/feature'

import './DetailsView.scss'

interface DetailsViewProps {
    feature: Feature
}

export const DetailsView: FunctionComponent<DetailsViewProps> = ({ feature }) => {

    return <div className="details-view">
        <div className="feature-content">
            <h3>{feature.title}</h3>
            {feature.details.map((group, i) => {
                return <div key={i} className="detail-group">
                    {group.map((detail, j) => {
                        return <div key={j} className="detail-line">
                            {detail}
                        </div>
                    })}
                </div>
            })}
        </div>
    </div>
}
import React, { FunctionComponent, KeyboardEventHandler, useEffect, useState } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { Data } from '../../types/data'
import { Feature } from '../../types/feature'
import { Folder } from '../../types/folder'

import './TreeView.scss'


const Title: FunctionComponent<{ onSelect: () => void, active?: boolean }> = ({ onSelect, active, children }) => {

    const onKeypress: KeyboardEventHandler = e => {
        if (e.key === 'Enter' || e.key === ' ') {
            onSelect()
        }
    }

    return <div className={'title ' + (active ? 'active' : '')} tabIndex={0} onClick={() => onSelect()} onKeyUp={onKeypress}>{children}</div>
}

interface TreeViewFeatureProps {
    feature: Feature
}

const TreeViewFeature: FunctionComponent<TreeViewFeatureProps> = ({ feature }) => {
    const history = useHistory()
    const matched = useRouteMatch<{ id: string }>()

    return <div className="feature">
        <Title onSelect={() => history.push('/features/' + encodeURIComponent(feature.title))} active={matched.params.id === feature.title}>📄 {feature.title}</Title>
    </div>
}

interface TreeViewFolderProps {
    folder: Folder,
    openAll: boolean
}

const TreeViewFolder: FunctionComponent<TreeViewFolderProps> = ({ folder, openAll }) => {
    const [open, setOpen] = useState(true)

    useEffect(() => {
        if (!open && openAll) {
            setOpen(true)
        }
    }, [openAll, open, setOpen])

    return <div className="folder">
        <Title onSelect={() => setOpen(!open)}>{open ? '📂' : '📁'} {folder.title}</Title>
        <ul style={{ display: open ? 'block' : 'none' }}>
            {folder.folders.map((f, i) => <li key={i}><TreeViewFolder folder={f} openAll={openAll} /></li>)}
            {folder.features.map((f, i) => <li key={i}><TreeViewFeature feature={f} /></li>)}
        </ul>
    </div>
}

interface TreeViewProps {
    data: Data,
    openAll: boolean
}

export const TreeView: FunctionComponent<TreeViewProps> = ({ data, openAll }) => {
    return <div className="tree-view">
        <TreeViewFolder folder={{ ...data, features: [] }} openAll={openAll} />
    </div>
}
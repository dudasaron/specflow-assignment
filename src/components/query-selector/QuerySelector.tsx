import React, { FunctionComponent } from 'react'
import { Query } from '../../types/query'

interface QuerySelectorParams {
    savedQueries: Query[]
    onQuerySelected: (filter: string) => void
}

export const QuerySelector: FunctionComponent<QuerySelectorParams> = ({ savedQueries, onQuerySelected }) => {
    return <select onChange={e => onQuerySelected(e.target.value)}>
        {savedQueries.map((query, index) => {
            return <option key={index} value={query.filter}> {query.name} </option>
        })}
    </select>
}
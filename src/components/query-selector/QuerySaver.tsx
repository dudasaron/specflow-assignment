import React, { FunctionComponent, useState } from 'react'
import { Query } from '../../types/query'


interface QuerySaverProps {
    filter: string
    onQuerySaved: (query: Query) => void
}

export const QuerySaver: FunctionComponent<QuerySaverProps> = ({ filter, onQuerySaved }) => {

    const [saveStarted, setSaveStarted] = useState(false)
    const [name, setName] = useState('')

    const saveQuery = () => {
        onQuerySaved({ name, filter })
        setSaveStarted(false)
    }

    return saveStarted ? <div>
        <input type="text" placeholder="Query name" onChange={e => setName(e.target.value)} />
        <button onClick={saveQuery} >Save</button>
    </div> : <button onClick={() => setSaveStarted(true)}>Save filter</button>
}
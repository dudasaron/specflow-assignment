import React, { FunctionComponent, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { mockData } from '../../mocks/mock-data'
import { getFilteredData } from '../../services/filter-data'
import { findFeature } from '../../services/find-feature'
import { Query } from '../../types/query'
import { DetailsView } from '../details-view/DetailsView'
import { FilterBar } from '../filter-bar/FilterBar'
import { QuerySaver } from '../query-selector/QuerySaver'
import { QuerySelector } from '../query-selector/QuerySelector'
import { TreeView } from '../tree-view/TreeView'

import './FeatureBrowser.scss'

export const FeatureBrowser: FunctionComponent = () => {

    const history = useHistory()

    const [savedQueries, setSavedQueries] = useState([] as Query[])

    const [filter, setFilter] = useState('')
    const data = getFilteredData(filter)

    const { id } = useParams<{ id: string }>()
    const selectedFeature = id ? findFeature(id, data) : undefined
    const featureExists = !!findFeature(id, mockData)

    if (!selectedFeature && featureExists) {
        history.push('/features')
    }

    return <div className="feature-browser">
        <div className="filter-row">
            <FilterBar value={filter} onFilterChanged={value => setFilter(value)} />
            <QuerySaver filter={filter} onQuerySaved={query => setSavedQueries([...savedQueries, query])} />
            <QuerySelector savedQueries={savedQueries} onQuerySelected={filter => setFilter(filter)} />
        </div>
        <div className="content-row">
            <TreeView data={data} openAll={!!filter} />
            {(id) && (selectedFeature ? <DetailsView feature={selectedFeature} /> : featureExists ? null : <h3 className="error">Feature not found!</h3>)}
        </div>
    </div>
}
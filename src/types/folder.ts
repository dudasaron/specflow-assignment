import { Feature } from "./feature";

export interface Folder {
    title: string;
    folders: Folder[];
    features: Feature[];
}
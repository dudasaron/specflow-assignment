import { Folder } from "./folder";

export interface Data {
    title: string;
    folders: Folder[];
}
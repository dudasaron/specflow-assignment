import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { FeatureBrowser } from './components/feature-browser/FeatureBrowser';

function App() {

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/features/:id?" component={FeatureBrowser} />
          <Route><Redirect to="/features" /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

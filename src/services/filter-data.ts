import { mockData } from "../mocks/mock-data";
import { Data } from "../types/data";
import { Feature } from "../types/feature";
import { Folder } from "../types/folder";

function filterFeatures(features: Feature[], filter: string): Feature[] {
    return features.filter(feature => feature.title.toUpperCase().includes(filter))
}

function filterFolders(folders: Folder[], filter: string): Folder[] {
    return filter === '' ? folders : folders.map(folder => ({
        title: folder.title,
        folders: filterFolders(folder.folders, filter),
        features: filterFeatures(folder.features, filter)
    })).filter(folder => folder.folders.length || folder.features.length)
}


export function getFilteredData(filter: string): Data {
    filter = filter.toUpperCase()

    return {
        title: mockData.title,
        folders: filterFolders(mockData.folders, filter)
    }

}

import { Data } from "../types/data"
import { Feature } from "../types/feature"
import { Folder } from "../types/folder"


function find(id: string, folders: Folder[]): Feature | undefined {
    for (const folder of folders) {
        const feature = folder.features.find(f => f.title === id) || find(id, folder.folders)

        if (feature) {
            return feature
        }
    }
}

export function findFeature(id: string, data: Data): Feature | undefined {
    return find(id, data.folders)
}
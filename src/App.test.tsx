import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import App from './App';
import { mockData } from './mocks/mock-data';

function getFeatureRegExp(title: string): RegExp {
  return new RegExp('📄\\s*' + title)
}

function getWildCardRegExp(search: string): RegExp {
  return new RegExp(search)
}

beforeEach(() => {
  // @ts-ignore
  delete window.location;
  // @ts-ignore
  window.location = new URL('http://localhost/');
});

test('renders the filter and tree by default and redirects to the /features path', () => {
  // Arrange
  render(<App />);

  // Assert
  const filterInput = screen.getByPlaceholderText('Filter by Keyword')
  const dataRoot = screen.getByText(getWildCardRegExp(mockData.title))
  expect(filterInput).toBeInTheDocument();
  expect(dataRoot).toBeInTheDocument();
  expect(document.location.pathname).toBe('/features')
})


test('opens the details view on click', () => {
  // Arrange
  render(<App />);
  const testFeatureName = 'Specification'
  const testContent = 'specifications'
  const featureNode = screen.getByText(getFeatureRegExp(testFeatureName))

  // Act
  featureNode.click()

  // Assert
  const featureNodes = screen.getAllByText(getWildCardRegExp(testFeatureName))
  const details = screen.getByText(testContent)
  expect(featureNodes).toHaveLength(2)
  expect(details).toBeInTheDocument()
  expect(document.location.pathname).toBe('/features/' + testFeatureName)


})


test('filter hides not matching features', () => {
  // Arrange
  render(<App />);
  const filterInput = screen.getByPlaceholderText('Filter by Keyword') as HTMLInputElement
  const foundFeature = "Tree view features"
  const filteredFeature = "Details view features"

  // Act
  fireEvent.change(filterInput, { target: { value: foundFeature } })

  // Assert
  expect(filterInput.value).toBe(foundFeature)
  expect(screen.getByText(getFeatureRegExp(foundFeature))).toBeInTheDocument()
  expect(screen.queryByText(getFeatureRegExp(filteredFeature))).toBeNull()

})


test('reset filter shows all items again', () => {
  // Arrange
  render(<App />);
  const filterInput = screen.getByPlaceholderText('Filter by Keyword') as HTMLInputElement
  const resetFilterButton = screen.getByTestId('resetFilterButton') as HTMLButtonElement
  const foundFeature = "Tree view features"
  const filteredFeature = "Details view features"

  // Act
  fireEvent.change(filterInput, { target: { value: foundFeature } })
  resetFilterButton.click()

  // Assert
  expect(filterInput.value).toBe('')
  expect(screen.getByText(getFeatureRegExp(foundFeature))).toBeInTheDocument()
  expect(screen.getByText(getFeatureRegExp(filteredFeature))).toBeInTheDocument()
})


test('clicking on folder should hide its sub-tree', () => {
  // Arrange
  render(<App />);
  const folderNode = screen.getByText(getWildCardRegExp("Documentation"))

  // Act
  folderNode.click()

  // Assert
  expect(folderNode.parentElement?.querySelector('ul')).not.toBeVisible()
})


test('on invalid route error message should be shown', () => {
  // Arrange
  // @ts-ignore
  window.location = new URL('http://localhost/features/bogus')
  render(<App />);

  // Assert
  expect(screen.getByText("Feature not found!")).toBeInTheDocument()
})

test('enter keypress should also trigger open/close folder', () => {
  // Arrange
  render(<App />);
  const folderNode = screen.getByText(getWildCardRegExp("Documentation"))

  // Act
  fireEvent.keyUp(folderNode, { key: 'Enter', code: 'Enter' })

  // Assert
  expect(folderNode.parentElement?.querySelector('ul')).not.toBeVisible()
})

test('space keypress should also trigger open/close folder', () => {
  // Arrange
  render(<App />);
  const folderNode = screen.getByText(getWildCardRegExp("Documentation"))

  // Act
  fireEvent.keyUp(folderNode, { key: ' ', code: ' ' })

  // Assert
  expect(folderNode.parentElement?.querySelector('ul')).not.toBeVisible()
})
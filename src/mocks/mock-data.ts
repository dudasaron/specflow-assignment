import { Data } from "../types/data";

export const mockData: Data = {
    title: 'Living Doc Assignment',
    folders: [
        {
            title: 'Features',
            folders: [
                {
                    title: "Filter bar",
                    folders: [],
                    features: [{
                        title: "Filter bar features",
                        details: [[
                            "Scenario: Clearing the search bar",
                            "Given that the filter input contains a search term",
                            "When the user clicks on the clear button",
                            "Then the Filter bar should reset the input to be empty"
                        ]]
                    }]
                },
                {
                    title: "Tree view",
                    folders: [],
                    features: [{
                        title: "Tree view features",
                        details: [[
                            "Scenario: Collapsing a folder in the tree view",
                            "Given that the tree has a folder with features",
                            "And the folder is open in the tree view",
                            "When the user clicks on the folders row",
                            "Then the folder should be closed",
                            "And the features should not be visible"
                        ]]
                    }]
                },
                {
                    title: "Details view",
                    folders: [],
                    features: [{
                        title: "Details view features",
                        details: [[
                            "Scenario: Viewing the details of a feature",
                            "Given that the tree contains a feature",
                            "When the user clicks on that feature",
                            "Then the Details view should show the title and details of the selected feature"
                        ]]
                    }]
                },
            ],
            features: []
        },
        {
            title: "Documentation",
            folders: [],
            features: [
                {
                    title: "Specification",
                    details: [
                        ["This", "contains"],
                        ["the", "specifications"],
                        ["of", "the", "project"]
                    ]
                }
            ]
        }
    ]
}
